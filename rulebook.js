import { WAMethods } from './module/waMethods.js';

/**
 * Initialization actions taken on Foundry Virtual Tabletop client init.
 */
 Hooks.once("init", () => {

  console.log('World-Anvil-Rulebook | Initializing World Anvil Rulebook Module');
  
  // Register the World Anvil module
  const module = game.modules.get("world-anvil");
  module.helpers = new WAMethods();
});

/**
 * When rendering WA Journal Entry, make sure to scroll to the correct anchor
 */
Hooks.on("renderJournalSheet", (app, html, data) => {
  const entry = app.object;
  const articleId = entry.getFlag("world-anvil", "articleId");
  if ( !articleId ) return;

  // Scroll to registered anchor if needed
  const waHelpers = game.modules.get("world-anvil").helpers;
  waHelpers.scrollToRegisteredAnchor(articleId, app);
});
