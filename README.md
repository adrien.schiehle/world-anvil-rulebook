# Foundry Virtual Tabletop - World Anvil Integration - Rulebook links

This module provides a way to directly link WA Journal Entry inside your system Achor sheet, Item sheet or other custom Applications.

> It will only be useful if you intend to create some custom module or game system.
> If you only want to manually link an already imported entry inside another one, the classic drag & drop provided by Foundry will be enough.

> This module is especially useful if you world in World Anvil contains your game system rules. It will allow you to easily put links inside your custom sheets.

-----

## Installation

This module can be installed by using the following module manifest url: (https://gitlab.com/adrien.schiehle/world-anvil-rulebook/-/raw/main/module.json).

-----

## Features

All provided feature are available by getting the helper :
~~~js
const helpers = game.modules.get("world-anvil").helpers;
~~~

### Linking articles from other parts of your system

You may need to add some links to your WA entries inside other sheets, like your items sheets.

For that, you will need to retrieve your articleId from world-anvil api. (Or just F12 while synchronizing your article).

With it, you will be able to link to your entry inside your other sheets :
~~~html
<span class="wa-link" data-article-id="<your article id>">Link description</span>
~~~

In order to trigger the links when clicking :
~~~js
/** @override */
activateListeners(html) {
    super.activateListeners(html);
    const helpers = game.modules.get("world-anvil").helpers;
    html.find('.wa-link').click(event => helpers.displayWALink(event));
    // [...]
}
~~~

### Using shortcuts

If there are journal entries that you link very often, you can use this syntax instead :
~~~html
<span class="wa-link" data-shortcut="spells.fireball"> </span>
~~~

It will allow you to store the label, article id, and anchor (optionaly) in only one place.

If you use this method, you will need to use this

~~~js
/** @override */
activateListeners(html) {
    super.activateListeners(html);
    const helpers = game.modules.get("world-anvil").helpers;
    helpers.substituteShortcutsAndHandleClicks(html);
    // [...]
}

// And Once :
game.modules.get("world-anvil").helpers.registerShortcuts({
    spells : {
        fireball: {
            label: 'Fireball',
            articleId: '3a11f3e5-bdfc-3970-bbdb-ea58f6d303e7',
            anchor: 'fireball' // Anchors are optional. They allow the journal entry to scroll to the right paragraph
        },
        bless: {
            label: 'Bless',
            articleId: '3a11f3e5-bdfc-3870-bbdb-ea58f6d303e7'
        }
    }
});
~~~

### Using anchors

Inside World Anvil, use anchor inside your article.

With previous example, the fireball paragraph shoud be written like this in WA:
~~~
[h2|fireball]The paragraph I want to link[/h2]
~~~

I personally use this to directly link rules chapters inside my character and item sheets :

![Preview](/docs/README_character_sheet_link.png?raw=true)



### Storing shortcuts without labels substitutions

If you don't want your labels on every links, you can use multiple css classes and bind different behaviors :

~~~js
/** @override */
activateListeners(html) {
    super.activateListeners(html);

    const helpers = game.modules.get("world-anvil").helpers.;
    helpers.substituteShortcutsAndHandleClicks(html, {classes: ['.wa-link']});
    helpers.substituteShortcutsAndHandleClicks(html, {classes: ['.wa-tooltip'], changeLabels: false});
    // [...]
}
~~~

This way, `wa-link` elements will have their labels replaced. And `wa-tooltip` elements won't.

### Shortcut childs

Sometimes, you will have a big article with multiple referenced paragraphs

You may not want to repeat label, articleId and anchor on each nodes of your shortcuts.

You can then store all the repeating data in a parent node.

~~~js
// Sortcut declaration
const shortcut = {
    spells : {
        fireball: {
            label: 'Fireball',
            articleId: '3a11f3e5-bdfc-3970-bbdb-ea58f6d303e7',
            components: {
                label: 'Needed components',
                anchor: 'spell_components'
            },
            upgrades: {
                selective_fireball: {
                    anchor: 'selective'
                },
                larger_one: {
                    anchor: 'larger'
                }
            }
        }
    }
};
~~~

By doing so, you can use those links :

~~~html
<span class="wa-link" data-shortcut="spells.fireball"> </span>
<span class="wa-link" data-shortcut="spells.fireball" data-child="components"> </span>
<span class="wa-link" data-shortcut="spells.fireball" data-child="upgrades.selective_fireball"> </span>
<span class="wa-link" data-shortcut="spells.fireball" data-child="upgrades.larger_one"> </span>
~~~

They will all link to the article `3a11f3e5-bdfc-3970-bbdb-ea58f6d303e7`, but some will scroll to a specific paragraph (`spell_components`, `selective`, `larger`), while some others will have a different label (`Needed components`)

### Using this with internationalization

I personnaly use shortcuts to link to differents articles depending on the currrent language.

The trick is to register different shortcuts during initialization phase :

~~~js
// Simplified version
export const initShortcuts = () => {
    let fireballShortcut;
    if( game.i18n.lang == 'fr' ) {
        fireballShortcut = { 
            spells : {
                fireball: {
                    label: 'Boule de feu',
                    articleId: '4b22f3e5-bdfc-3970-bbdb-ea58f6d303e7'
                }
        };
    } else {
        fireballShortcut = { 
            spells : {
                fireball: {
                    label: 'Fireball',
                    articleId: '3a11f3e5-bdfc-3970-bbdb-ea58f6d303e7',
                    anchor: 'fireball'
                }
        };
    }

    const helpers = game.modules.get("world-anvil").helpers.;
    helpers.registerShortcuts(fireballShortcut);
}
~~~

This way, when i use the shortcut link, French browser will link to the french article. (This one seems to have no anchor).

Other browsers will link to the english version of the article.

I found it usefull for linking custom game rules.