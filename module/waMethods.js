
/**
 * Open existing Journal entry or try to import a new new if the article Id is not yet known
 * @param {String} articleId : WA article Id
 * @returns The journalEntry
 * @throws Error if not permitted
 */
 const openJournalEntryFromArticleId = async (articleId) => {

    if(! articleId || articleId == '') { throw game.i18n.localize("WARulebook.NoPermissionView"); }
  
    // View an existing linked article (OBSERVER+)
    const entry = game.journal.find(e => e.getFlag("world-anvil", "articleId") === articleId);
    if ( entry ) {
      if ( !entry.testUserPermission(game.user, "OBSERVER") ) { throw game.i18n.localize("WARulebook.NoPermissionView"); }
  
      await entry.sheet.render(true);
      return entry;
    }
  
    // Import a new article (GM Only)
    if ( !game.user.isGM ) { throw game.i18n.localize("WARulebook.NoPermissionView"); }

    const coreModule = game.modules.get("world-anvil");
    module.api.importArticle(articleId, {renderSheet: true});
  }
  
  /**
   * Methods for managing shortcuts inside custom sheets
   */
  export class WAMethods {
  
    get shortcutsInserted() {
      return this._shortcutsInserted ?? false;
    }
    
    get shortcuts() {
      // Waiting for i18 initialization. Seems like I've initialized it a little too soon
      if(! this._shortcuts && game.i18n.has('WARulebook.shortcut.default')) { 
        
        const defaultShortcut = {
          label: game.i18n.localize('WARulebook.shortcut.default'),
          articleId: '',
          classes: ['not-found']
        }
  
        const noShortcuts = {
          label: '[...]',
          articleId: ''
        };
        
        this._shortcuts = {
          default: defaultShortcut,
          noShortcuts: noShortcuts
        };
      }
      return this._shortcuts;
    }
  
    /** Map of registered anchors that should be displayed once the sheet is rendered. */
    get registeredAnchors() {
  
      if(!this._registeredAnchors) { this._registeredAnchors = new Map(); }
      return this._registeredAnchors;
    }
  
    /**
     * _shortcuts structure :
     * {
     *  path: {
     *    to: {
     *      shortcut: {
     *        articleId: 'Link to WA article',
     *        label: 'displayed Name',  // Optionnal. If null or empty, will be ignored
     *        classes : []              // Optionnal. Allow to add css class on the fly
     *        path: {
     *          to: {
     *            child: {              // A shortcut can have multiple childs. Child may override label, articleId and anchor
     *               anchor: 'Id of your paragraph',
     *               label: 'displayed Name when anchor is used',  // Optionnal. If null or empty, will be ignored
     * [...] }
     * @typedef {label: string, articleId: string} WAShortcut
     * @param {WAShortcut} newShortcuts 
     */
    registerShortcuts(newShortcuts) {
      if(! this.shortcuts ) { throw 'Registering too soon. Translation file not yet loaded'; }
      this._shortcutsInserted = true;
      this._shortcuts = mergeObject(this.shortcuts, newShortcuts);
    }
  
    substituteShortcutsAndHandleClicks(html, {classes=['.wa-link'], changeLabels=true}={}) {
      classes.forEach(c => {
        if( changeLabels ) { this.fillShortcutNames(html, c); }
        html.find(c).click(event => this.displayWALink(event));
      });
    }
  
    fillShortcutNames(html, shortcutClass='.wa-link') {
      
      const links = Array.from(html.find(shortcutClass));
      links.forEach(link => {
        const shortcutPath = link.dataset.shortcut;
        const childPath = link.dataset.child;
        if(shortcutPath) {
          const shortcut = this.articleFromShortcut(shortcutPath, childPath);
  
          if(shortcut.label && shortcut.label != '') { link.textContent = shortcut.label; }
          if(shortcut.classes && shortcut.classes instanceof Array) { shortcut.classes.forEach(c => link.classList.add(c)); }
        }
      });
    }
  
    async displayWALink(event) {
      event.preventDefault();
      const dataset = event.currentTarget.dataset;
      const shortcutPath = dataset.shortcut;
      const childPath = dataset.child;
  
      // Data-shortcut takes priority
      const defaultShortcut = { articleId: dataset.articleId };
      const shortcut = shortcutPath ? this.articleFromShortcut(shortcutPath, childPath) : defaultShortcut;
  
      // Get WA Article
      try {
        const journalEntry = await openJournalEntryFromArticleId(shortcut.articleId);
        
        if( shortcut.anchor) { 
          this.registeredAnchors.set(shortcut.articleId, shortcut.anchor);
          await journalEntry.sheet.render(true); //Render again
        }
  
      } catch(e) {
        return ui.notifications.warn(e);
      }
    }
  
    articleFromShortcut(shortcutPath, childPath) {
    
      if( !this.shortcutsInserted ) {return getProperty(this.shortcuts, 'noShortcuts'); }
      const shortcut = getProperty(this.shortcuts, shortcutPath);
    
      if( !shortcut ) { return getProperty(this.shortcuts, 'default'); }
  
      const child = !childPath ? null : getProperty(shortcut, childPath);
  
      return {
        label: child?.label ?? shortcut.label,
        articleId: child?.articleId ?? shortcut.articleId,
        anchor: child?.anchor ?? shortcut.anchor
      };
    }
    
    
  
    scrollToRegisteredAnchor(articleId, entrySheet) {
      const anchorId = this.registeredAnchors.get(articleId);
      if( ! anchorId ) { return; }
  
      const document = entrySheet.element[0];
      let scrollToElement = document.querySelector('#'+anchorId);
      if( !scrollToElement ) {
        const info = game.i18n.localize('WARulebook.shortcut.noAnchor').replace( 'ANCHOR', anchorId );
        return ui.notifications.info( info );
      }
  
      const containerHeight = document.clientHeight;
  
      const movableNode = scrollToElement.parentNode.parentNode;
  
      if (movableNode.scrollTop + containerHeight < scrollToElement.offsetTop) {
        movableNode.scrollTop = scrollToElement.offsetTop;
  
      } else if ( scrollToElement.offsetTop < movableNode.scrollTop) {
        movableNode.scrollTop = scrollToElement.offsetTop;
      };
  
      this.registeredAnchors.delete(articleId);
    }
  }